<?php
	//http://codular.com/curl-with-php
	//http://www.lornajane.net/posts/2012/using-oauth2-for-google-apis-with-php
	//http://stackoverflow.com/questions/4803948/curl-exec-printing-results-when-i-dont-want-to
	if(isset($_GET['code'])) {
		// try to get an access token
		$code = $_GET['code'];
		$url = 'https://accounts.google.com/o/oauth2/token';
		$client_id = getenv('aurum_client_id');
		$client_secret = getenv('aurum_client_secret');
		$environment = getenv('aurum_environment');	
		if(($environment !== "local" && $environment !== "production") || !isset($client_id) || !isset($client_secret)){
			echo "Please ensure that the following environment variables are set: aurum_client_id, aurum_client_secret and aurum_environment";
		}
		else{
			$redirect_uri;
			if($environment === "local"){
				$redirect_uri = "http://localhost/php/user.php";
			}	
			else if($environment === "production"){
				$redirect_uri = "https://aurum-web.herokuapp.com/php/user.php";
			}
			$params = array(
			"code" => $code,
			"client_id" => $client_id,
			"client_secret" => $client_secret,
			"redirect_uri" => $redirect_uri,
			"grant_type" => "authorization_code"
			);

			$request = curl_init();
			curl_setopt($request, CURLOPT_URL, $url);
			curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($params));
			curl_setopt($request, CURLOPT_POST, 1);
			curl_setopt($request, CURLOPT_RETURNTRANSFER, TRUE);
			$responseObj = curl_exec($request);
			if(curl_errno($request) != 0){
				echo "Error: " . curl_error($request);
			}
			echo "Access token: " . json_decode($responseObj)->access_token;
			echo "Database URL: " . createUser();
		}
	}
	function createUser(){
		$url = getenv('DATABASE_URL');
		return $url;
	}
?>
