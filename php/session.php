<?php
	$url = "https://accounts.google.com/o/oauth2/auth";
	$client_id = getenv('aurum_client_id');
	$client_secret = getenv('aurum_client_secret');

	$environment = getenv('aurum_environment');	
	if(($environment !== "local" && $environment !== "production") || !isset($client_id) || !isset($client_secret)){
		echo "Please ensure that the following environment variables are set: aurum_client_id, aurum_client_secret and aurum_environment";
	}
	else{
		$redirect_uri;
		if($environment === "local"){
			$redirect_uri = "http://localhost/php/user.php";
		}	
		else if($environment === "production"){
			$redirect_uri = "https://aurum-web.herokuapp.com/php/user.php";
		}
		$params = array(
			"response_type" => "code",
			"client_id" => $client_id,
			"redirect_uri" => $redirect_uri,
			"scope" => "https://www.googleapis.com/auth/plus.me"
			);
		$request_to = $url . '?' . http_build_query($params);
		header("Location: " . $request_to);
	}
?>
