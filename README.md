#Requirements
PHP and Apach, with the oauth and curl extensionse

#Necessary Deployment Environment Variables:
Linux:
	SetEnv aurum_client_id "<client_id_from_google>"
	SetEnv aurum_client_secret "<client_secret_from_google>"
	SetEnv aurum_environment "<either set to local or production">

*The above are used for OAuth*
You can set them for Heroku:
	heroku config:set aurum_environment=production
	heroku config:set aurum_client_id=<client_id>
	heroku config:set aurum_client_secret=<client_secret>

#Local Database:

Install postgres. Then, create a database:
	export DATABASE_URL=postgres:///$(whoami)
will set an environtment variable for the db URL
	printenv | grep DATABASE_URL
will return the db URL for aurum to use locally. Add the following for aurum to be able to read it
	SetEnv DATABASE_URL "<db_url>"

	$ psql -U postgres
	psql (9.5.1)
	Type "help" for help.

	postgres=# CREATE DATABASE aurum;
	CREATE DATABASE
	postgres=# \c aurum;
	You are now connected to database "aurum" as user "postgres".
	CREATE TABLE users("id" SERIAL,"name" VARCHAR(50), "email" VARCHAR(50), "token" VARCHAR(2000));
	CREATE TABLE
	aurum=# SELECT * FROM users;
	 id | name | email | token 
	----+------+-------+-------
	(0 rows)


